var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var campgrounds = [
    {name : "Mansarovar", image : "https://cdn.pixabay.com/photo/2016/02/18/22/16/tent-1208201__340.jpg"},
    {name : "Pangong Tso", image: "https://cdn.pixabay.com/photo/2016/02/18/22/16/tent-1208201__340.jpg"},{name : "Pangong Tso", image: "https://cdn.pixabay.com/photo/2016/02/18/22/16/tent-1208201__340.jpg"},{name : "Mansarovar", image : "https://cdn.pixabay.com/photo/2016/02/18/22/16/tent-1208201__340.jpg"},
    {name : "Pangong Tso", image: "https://cdn.pixabay.com/photo/2016/02/18/22/16/tent-1208201__340.jpg"},{name : "Pangong Tso", image: "https://cdn.pixabay.com/photo/2016/02/18/22/16/tent-1208201__340.jpg"}
 
  ];

app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");

app.get('/',function(req, res){
  res.render("landing");
});

app.get('/campgrounds', function(req, res){
    res.render("campgrounds", {campgrounds:campgrounds});
});

app.post('/campgrounds', function(req, res){
  //get data from forms and add to campgrounds
  
  var name = req.body.name;
  var image = req.body.image;
  var newCampGround = {name :name, image : image};
  campgrounds.push(newCampGround);
  res.redirect('/campgrounds');
});

app.get('/campgrounds/new', function(req, res){
  res.render("new.ejs");
});


app.listen(8000, function(){
  console.log("Server up and running successfully!");
    
});
