let states = ["Kansas", "Nebraska", "North Dakota","South Dakota"];

//urls Imperative version
// function imperativeUrls(elements) {
//   let urls = [];
//   elements.forEach((element) => {
//     urls.push(element.toLowerCase().split(/\s+/).join("-"));
//   });
//   return urls;
// }
// console.log(imperativeUrls(states));

//Cleaning the string
let urlify = (string) =>{
  return string.toLowerCase().split(/\s+/).join('-');
}

//Using Map
let functinalUrls = (elements)=>{
  return elements.map(element => urlify(element))
}

let urlBuild = (links) => {
  let res = functinalUrls(links);
  res.forEach((item) => {
    console.log("https://www.example.com/"+item);
  });
}
urlBuild(states);
