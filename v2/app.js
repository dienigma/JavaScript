var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var mongoose = require("mongoose");

mongoose.connect("mongodb://localhost/yelp_camp");

//schema setup
var campgroundschema = new mongoose.Schema({
  name: String,
  image : String,
  description : String
});

var Campground = mongoose.model("Campground", campgroundschema);

Campground.create({name : "Mansarovar", image : "https://cdn.pixabay.com/photo/2016/02/18/22/16/tent-1208201__340.jpg", description : "This is a famous lake and a hiking site in Himalayas."}, function(err, campground){
 if (err){
 console.log(err);
 }else{
  console.log(campground);
 }
});


app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");

app.get('/',function(req, res){
  res.render("landing");
});


//Index route.
app.get('/campgrounds', function(req, res){
   Campground.find({},function(err, allcampgrounds){
    if(err){
      console.log(err);
    } else{
      res.render("campgrounds",{campgrounds:allcampgrounds});
    }
   
   
   });
   
   // res.render("campgrounds", {campgrounds:campgrounds});
});

//post page
app.post('/campgrounds', function(req, res){
  //get data from forms and add to campgrounds
  
  var name = req.body.name;
  var image = req.body.image;
  var newCampGround = {name :name, image : image};
 // campgrounds.push(newCampGround);
  Campground.create(newCampGround,function(err, newlyCreated){
    if(err){
    console.log(err)
    } else{
       res.redirect('/campgrounds');
 
    }
  });
  
 });

//new route
app.get('/campgrounds/new', function(req, res){
  res.render("new.ejs");
});

//Show route
app.get('/campgrounds/:id', function(req, res){
  res.send("This is the show page");
});
app.listen(8000, function(){
  console.log("Server up and running successfully!");
    
});
